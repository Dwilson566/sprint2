﻿using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using WebApp1.Services;
using Xunit;

namespace UnitTest.Services
{
    public class EmailSenderTests
    {
        public IOptions<AuthMessageSenderOptions> options { get; private set; }

        [Fact]
        public async Task SendEmailAsync_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var emailSender = new EmailSender(options);
            string email = null;
            string subject = null;
            string message = null;

            // Act
            await emailSender.SendEmailAsync(
                email,
                subject,
                message);

            // Assert
            Assert.True(false);
        }

        [Fact]
        public async Task Execute_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var emailSender = new EmailSender(options);
            string apiKey = null;
            string subject = null;
            string message = null;
            string email = null;

            // Act
            await emailSender.Execute(
                apiKey,
                subject,
                message,
                email);

            // Assert
            Assert.True(false);
        }
    }
}
