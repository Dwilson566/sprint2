﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebApp1;

namespace UnitTest
{
    [TestClass]
    public class StartupTests
    {
        [TestMethod]
        public void ConfigureServices_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var startup = new Startup();
            IServiceCollection services = null;

            // Act
            startup.ConfigureServices(
                services);

            // Assert
            Assert.Fail();
        }

        [TestMethod]
        public void Configure_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var startup = new Startup();
            IApplicationBuilder app = null;
            IWebHostEnvironment env = null;

            // Act
            startup.Configure(
                app,
                env);

            // Assert
            if (startup != null)
            {
                Assert.IsFalse(false);
            }
        }
    }
}
