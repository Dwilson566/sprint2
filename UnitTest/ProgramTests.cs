﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WebApp1;

namespace UnitTest
{
    [TestClass]
    public class ProgramTests
    {
        [TestMethod]
        public void Main_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var program = new Program();
            string[] args = null;

            // Act
            Program.Main(
                args);

            // Assert
            Assert.Fail();
        }

        [TestMethod]
        public void CreateHostBuilder_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var program = new Program();
            string[] args = null;

            // Act
            var result = Program.CreateHostBuilder(
                args);

            // Assert
            Assert.Fail();
        }
    }
}
