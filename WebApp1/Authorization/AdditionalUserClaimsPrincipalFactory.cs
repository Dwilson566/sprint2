using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using WebApp1.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
 
 

namespace WebApp1.Authorization
{
  public class AdditionalUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<WebApp1User, IdentityRole>
{
	public AdditionalUserClaimsPrincipalFactory( 
		UserManager<WebApp1User> userManager,
		RoleManager<IdentityRole> roleManager, 
		IOptions<IdentityOptions> optionsAccessor) 
		: base(userManager, roleManager, optionsAccessor)
	{}

	public async override Task<ClaimsPrincipal> CreateAsync(WebApp1User user)
	{
		var principal = await base.CreateAsync(user);
		var identity = (ClaimsIdentity)principal.Identity;
		

		var claims = new List<Claim>();
		if (user.IsAdmin)
		{
			claims.Add(new Claim(ClaimTypes.Role, "admin"));
		}
		else if (user.IsAbet)
		{
			claims.Add(new Claim(ClaimTypes.Role, "abetAccreditation"));
		}
		else if (user.IsProfessor)
		{
			claims.Add(new Claim(ClaimTypes.Role, "professor"));
		}
		else
		{
			claims.Add(new Claim(ClaimTypes.Role, "user"));
		}

		identity.AddClaims(claims);
		return principal;
	}
}
}