using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WebApp1.Data;
using WebApp1.Authorization;
using WebApp1.Areas.Identity.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using WebApp1.Services;
using System.Security.Claims;

namespace WebApp1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public Startup()
        {
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
           services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDatabaseDeveloperPageExceptionFilter();
            services.AddDefaultIdentity<WebApp1.Areas.Identity.Data.WebApp1User>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddTransient<IEmailSender, EmailSender>();
            services.Configure<AuthMessageSenderOptions>(Configuration);
                            
         
            services.AddRazorPages(options => {

                options.Conventions.AuthorizeFolder("/");
                    

			});
            services.AddScoped<IUserClaimsPrincipalFactory<WebApp1User>,  AdditionalUserClaimsPrincipalFactory>();
            
           

           services.AddDbContext<OutcomesContext>(options =>
                    options.UseSqlite(Configuration.GetConnectionString("OutcomesContext")));

            services.AddAuthorization(options => 
            {
                options.AddPolicy("IsAdmin", policy => policy.RequireClaim("role", "admin"));
                options.AddPolicy("IsAbet", policy => policy.RequireClaim("role", "abetAccreditation"));
                options.AddPolicy("IsProfessor", policy => policy.RequireClaim("role", "professor"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
         private async Task ConfigureRoles(ServiceProvider serviceProvider)
		{

			var roleMgr = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
			var exists = await roleMgr.RoleExistsAsync("admin");
			if (!exists)
			{
				var admin = new IdentityRole
				{
					Name = "admin"
				};
				await roleMgr.CreateAsync(admin);
			}

			var adminRole = await roleMgr.FindByNameAsync("admin");
			var userMgr = serviceProvider.GetRequiredService<UserManager<WebApp1User>>();
			var user = await userMgr.FindByEmailAsync("administrator@test.com");

             //----------------------Abet Accredation -------------------------//
            var abetMgr = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
			var Abetexists = await abetMgr.RoleExistsAsync("abetAccreditation");
			if (!Abetexists)
			{
				var abetAcc = new IdentityRole
				{
					Name = "abetAccreditation"
				};
				await abetMgr.CreateAsync(abetAcc);
			}

			var abetRole = await abetMgr.FindByNameAsync("abetAccreditation");
			var abetuserMgr = serviceProvider.GetRequiredService<UserManager<WebApp1User>>();
			var abetuser = await abetuserMgr.FindByEmailAsync("abet@test.com");
             //----------------------Professor -------------------------//
            var ProMgr = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
			var Proexists = await ProMgr.RoleExistsAsync("professor");
			if (!Proexists)
			{
				var Pro = new IdentityRole
				{
					Name = "Professor"
				};
				await ProMgr.CreateAsync(Pro);
			}

			var ProRole = await ProMgr.FindByNameAsync("Professsor");
			var ProUserMgr = serviceProvider.GetRequiredService<UserManager<WebApp1User>>();
			var ProUser = await ProUserMgr.FindByEmailAsync("Professor@test.com");
    }
}
}
