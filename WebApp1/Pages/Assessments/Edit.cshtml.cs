using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebApp1.Data;
using WebApp1.Models;

namespace WebApp1.Pages.Assessments
{
    public class EditModel : PageModel
    {
        private readonly WebApp1.Data.OutcomesContext _context;

        public EditModel(WebApp1.Data.OutcomesContext context)
        {
            _context = context;
        }

        [BindProperty]
        public StudentAssessments StudentAssessments { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            StudentAssessments = await _context.StudentAssessments.FirstOrDefaultAsync(m => m.Id == id);

            if (StudentAssessments == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(StudentAssessments).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!StudentAssessmentsExists(StudentAssessments.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool StudentAssessmentsExists(int id)
        {
            return _context.StudentAssessments.Any(e => e.Id == id);
        }
    }
}
