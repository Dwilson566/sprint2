using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebApp1.Data;
using WebApp1.Models;
using WebApp1.Models.ViewModels;

namespace WebApp1.Pages.Assessments
{
    public class IndexModel : PageModel
    {
        private readonly WebApp1.Data.OutcomesContext _context;

        public IndexModel(WebApp1.Data.OutcomesContext context)
        {
            _context = context;
        }

        public IList<StudentAssessments> StudentAssessments { get;set; }
        public StudentOutcomesIndexData StudentAssessmentData { get; set; }

        public async Task OnGetAsync(String Course, String SelectedCourse)
        {
            StudentAssessmentData = new StudentOutcomesIndexData();

            if (Course != null) {
                StudentAssessmentData.StudentAssessments = await _context.StudentAssessments
                    .Where(i => i.Course == Course)
                        .ToListAsync();
            } else {
                StudentAssessmentData.StudentAssessments = await _context.StudentAssessments
                    .ToListAsync();
            }

            if (SelectedCourse != null) {
                StudentAssessmentData.CourseLearningOutcomes = await _context.CourseLearningOutcome
                    .Where(i => i.Course == SelectedCourse)
                        .ToListAsync();
            }
        }
    }
}
