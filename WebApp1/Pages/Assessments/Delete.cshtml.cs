using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebApp1.Data;
using WebApp1.Models;

namespace WebApp1.Pages.Assessments
{
    public class DeleteModel : PageModel
    {
        private readonly WebApp1.Data.OutcomesContext _context;

        public DeleteModel(WebApp1.Data.OutcomesContext context)
        {
            _context = context;
        }

        [BindProperty]
        public StudentAssessments StudentAssessments { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            StudentAssessments = await _context.StudentAssessments.FirstOrDefaultAsync(m => m.Id == id);

            if (StudentAssessments == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            StudentAssessments = await _context.StudentAssessments.FindAsync(id);

            if (StudentAssessments != null)
            {
                _context.StudentAssessments.Remove(StudentAssessments);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
