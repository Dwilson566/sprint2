using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using WebApp1.Models.ViewModels;
using WebApp1.Data;
using WebApp1.Models;

namespace WebApp1.Pages
{
    public class AssessmentCoursesModel : PageModel
    {
       private readonly WebApp1.Data.OutcomesContext _context;

        public AssessmentCoursesModel(WebApp1.Data.OutcomesContext context)
        {
            _context = context;
        } 

        public IList<StudentAssessments> StudentAssessments { get;set; }

        public async Task OnGetAsync()
        {
            StudentAssessments = await _context.StudentAssessments.ToListAsync();
        }
    }

}