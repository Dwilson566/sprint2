using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp1.Models
{
    public class StudentAssessments
    {
        public int Id { get; set; }
        public string Course { get; set; }
        public string Instructor { get; set; }
        public string Assessment { get; set; }
        [Display(Name = "Total Students")]
        public int TotalStudents { get; set; }

        [Display(Name = "Complete")]
        public int CompleteLevel { get; set; }

        [Display(Name = "Satisfactory")]
        public int SatisfactoryLevel { get; set; }

        [Display(Name = "Not Met")]
        public int NotMet { get; set; }

        [Display(Name = "Not Measurable")]
        public int NotMeasurable { get; set; } 

    }
}