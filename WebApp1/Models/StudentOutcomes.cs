using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp1.Models 
{

    public class StudentOutcome 
    {
        public int Id { get; set; }
        public string Program{ get; set; }
        
        [Display(Name = "Description")]
        public string StudentOutcomeDescription { get; set; }

        [Display(Name = "Total Students")]
        public int TotalStudents { get; set; }

        [Display(Name = "Complete")]
        public int CompleteLevel { get; set; }

        [Display(Name = "Satisfactory")]
        public int SatisfactoryLevel { get; set; }

        [Display(Name = "Not Met")]
        public int NotMet { get; set; }

        [Display(Name = "Not Measurable")]
        public int NotMeasurable { get; set; }       
    }
}