using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApp1.Models.ViewModels
{
    public class StudentOutcomesIndexData
    {
        public IEnumerable<StudentOutcome> StudentOutcome { get; set;}
        public IEnumerable<CourseLearningOutcome> CourseLearningOutcomes { get; set; }

        public IEnumerable<StudentAssessments> StudentAssessments { get; set; }
    }
}