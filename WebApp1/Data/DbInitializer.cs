using System;
using WebApp1.Models;
using System.Linq;

namespace WebApp1.Data
{
    public static class DbInitializer
    {
        public static void Initialize(OutcomesContext context)
        {
            if (context.CourseLearningOutcome.Any())
            {
                return;
            }

            var CourseLearningOutcomes = new CourseLearningOutcome[]
            {
                new CourseLearningOutcome{Program="CIDM",Course="4390",Instructor="Dr. Babb",CourseLearningOutcomeDescription="Learn to code",TotalStudents=40,CompleteLevel=25,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
                new CourseLearningOutcome{Program="CIDM",Course="4390",Instructor="Dr. Babb",CourseLearningOutcomeDescription="Learn Scrum",TotalStudents=40,CompleteLevel=25,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
                new CourseLearningOutcome{Program="Business",Course="4315",Instructor="Dr. Business",CourseLearningOutcomeDescription="Learn to Manage",TotalStudents=40,CompleteLevel=25,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
                new CourseLearningOutcome{Program="Business",Course="4315",Instructor="Dr. Business",CourseLearningOutcomeDescription="Learn to communicate",TotalStudents=40,CompleteLevel=25,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2}
            };

            context.CourseLearningOutcome.AddRange(CourseLearningOutcomes);
            context.SaveChanges();

            var StudentOutcomes = new StudentOutcome[]
            {
                new StudentOutcome{Program="CIDM",StudentOutcomeDescription="Analyze a complex computing problem and to apply principles of computing and other relevant disciplines to identify solutions.",TotalStudents=100,CompleteLevel=60,SatisfactoryLevel=20,NotMet=10,NotMeasurable=10},
                new StudentOutcome{Program="CIDM",StudentOutcomeDescription="Design, implement, and evaluate a computing-based solution to meet a given set of computing requirements in the context of the program’s discipline.",TotalStudents=100,CompleteLevel=60,SatisfactoryLevel=20,NotMet=10,NotMeasurable=10},
                new StudentOutcome{Program="Business",StudentOutcomeDescription="Learn to invest",TotalStudents=100,CompleteLevel=60,SatisfactoryLevel=20,NotMet=10,NotMeasurable=10},
                new StudentOutcome{Program="Business",StudentOutcomeDescription="Learn how the economy works",TotalStudents=100,CompleteLevel=60,SatisfactoryLevel=20,NotMet=10,NotMeasurable=10}
            };

            context.StudentOutcome.AddRange(StudentOutcomes);
            context.SaveChanges();

            var Assessments = new StudentAssessments[]
            {
                new StudentAssessments{Course="4390",Instructor="Dr. Babb",Assessment="Test 3",TotalStudents=30,CompleteLevel=15,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
                new StudentAssessments{Course="4390",Instructor="Dr. Babb",Assessment="Assignment 4",TotalStudents=30,CompleteLevel=15,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
                new StudentAssessments{Course="4315",Instructor="Dr. Business",Assessment="Final Exam",TotalStudents=30,CompleteLevel=15,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
                new StudentAssessments{Course="4315",Instructor="Dr. Business",Assessment="Final Project",TotalStudents=30,CompleteLevel=15,SatisfactoryLevel=10,NotMet=3,NotMeasurable=2},
            };

            context.StudentAssessments.AddRange(Assessments);
            context.SaveChanges();

        }
    }
}